//
//  EASFrame.swift
//  EAS Photo Booth
//
//  Created by Jim Webb on 9/2/15.
//  Copyright (c) 2015 B&T Inc. All rights reserved.
//

import UIKit

@IBDesignable
class EASFrame: UIView, UIScrollViewDelegate {
//    
//    CGRect newFrame = self.myview.frame;
//    
//    newFrame.size.width = 200;
//    newFrame.size.height = 200;
//    [self.myview setFrame:newFrame];
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.CGColor
        }
    }
    @IBInspectable var newBackgroundColor: UIColor? {
        didSet{
            layer.backgroundColor = newBackgroundColor?.CGColor
        }
    }
    
}