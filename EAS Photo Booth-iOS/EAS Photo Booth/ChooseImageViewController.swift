//
//  ChooseImageViewController.swift
//  EAS Photo Booth
//
//  Created by Jim Webb on 1/19/15.
//  Copyright (c) 2015 Jim Webb All rights reserved.
//

import UIKit
import MobileCoreServices

class ChooseImageViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, BLECentralDelegate {
    
    var bleCentralManager: BLECentralManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        captureProgressBar.setProgress(0.0, animated: false)
        captureProgressLabel.text = "Capture: 0.0%"
        bleCentralManager = BLECentralManager(delegate: self)
        bleCentralManager.delegate = self;
    }
    
    
    //MARK:  Outlet Declarations
    @IBOutlet weak var userImage: UIImageView!
    
    var testScreen = EASScreen()
    
    @IBOutlet weak var captureProgressBar: UIProgressView!
    
    @IBOutlet weak var captureProgressLabel: UILabel!
    
    @IBOutlet weak var opacityLabel: UILabel!
    
    @IBOutlet weak var redrawImageButton: UIButton!
    
    @IBOutlet weak var captureImageButton: UIButton!
    
    @IBOutlet weak var chooseImageButton: UIButton!
    
    @IBOutlet weak var opacitySlider: UISlider! {
        didSet {
            updateSliderValue(opacitySlider)
        }
    }
    
    @IBAction func updateSliderValue(sender: UISlider!){
            opacityLabel.text = "Opacity: \(Int(sender.value * 100))%"
    }
    
    //MARK:  Send to Redraw
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier {
            switch identifier {
                case "Show Redrawn Image":
                    if let vc = segue.destinationViewController as? RedrawImageViewController {
                        vc.incomingGrid = grid
                    }
                default: break
            }
        }
    }
    
    //MARK:  Capture Image
    
    var grid = [[CoOrd]](){
        didSet{
            if grid.count > 0 {
                redrawImageButton.enabled = true
            } else {
                redrawImageButton.enabled = false
                updateProgressBar(0)
            }
        }
    }
    
    @IBAction func createImageGrid(sender: AnyObject) {
        
        var progressCount = 0
        var imageComponent: (r: Double, g: Double, b: Double, a: Double)
        grid.removeAll(keepCapacity: false)
        print("At Capture: \(grid.count)")
        
        if let testImage = userImage?.image {
            for x in 0...Int(testScreen.maxWidth){  //testImage.size.width) {
                grid.append([CoOrd]())
                for y in 0...Int(testScreen.maxHeight){  //testImage.size.height) {
                    grid[x].append(CoOrd(x: x, y: y))
                    let xPos: CGFloat = CGFloat(x)
                    let yPos: CGFloat = CGFloat(y)
                    imageComponent = testImage.determineComponents(CGPointMake(xPos, yPos))
                    if ((imageComponent.r + imageComponent.g + imageComponent.b)/3) < Double(opacitySlider.value) {
                        grid[x][y].painted = true
                        grid[x][y].alreadyVisited  = false
                    } else {
                        grid[x][y].painted = false
                        grid[x][y].alreadyVisited  = false
                    }
                    //print("x: \(x), y: \(y), \(grid[x][y].painted)")

                    progressCount++
                    updateProgressBar(progressCount)
                    
                }
            }

        }
        print("Out of Capture: \(grid.count) x elements, \(grid[grid.count - 1].count) y elements")
    }
    
    func updateProgressBar(progressCount: Int) {
        dispatch_async(dispatch_get_main_queue()){
            //if let testImage = self.userImage?.image {
                let currentProgress = Float(progressCount)/Float((self.testScreen.maxWidth + 1) * (self.testScreen.maxHeight + 1)) //Float((testImage.size.width + 1) * (testImage.size.height + 1))
                self.captureProgressBar.setProgress(currentProgress, animated: false)
                self.captureProgressLabel.text = "Capture: \(currentProgress*100)%"
            //}
        }
    }
    
    //MARK: Update Photos
    
    func useCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.Camera) {
            let picker = UIImagePickerController()
            picker.sourceType = .Camera
            //picker.mediaTypes = [kUTTypeImage]
            picker.delegate = self
            picker.allowsEditing = true
            self.presentViewController(picker, animated: true, completion: nil)
        }
    }
    
    func usePhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.PhotoLibrary) {
            let picker = UIImagePickerController()
            picker.sourceType = .PhotoLibrary
            //picker.mediaTypes = [kUTTypeImage]
            picker.delegate = self
            picker.allowsEditing = true
            self.presentViewController(picker, animated: true, completion: nil)
        }
    }
    
    func didDiscoverConnection(connection: BLEConnection) {
        print("didDiscoverConnection")
        bleCentralManager.connect();
    }
    
    func didConnectConnection(connection: BLEConnection) {
        print("didConnectConnection")
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        self.userImage.image = nil
        grid.removeAll()
        
        let image = info[UIImagePickerControllerEditedImage] as? UIImage
        if image == nil {
            captureImageButton.enabled = false
        } else {
            captureImageButton.enabled = true
            self.userImage.image = image!.resizedImageWithBounds(CGSize(width: CGFloat(testScreen.maxWidth), height: CGFloat(testScreen.maxHeight)))
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func pickImage(sender: AnyObject) {
        let pickImageSheet: UIAlertController = UIAlertController(title: nil, message: "Choose an Image Source", preferredStyle: .ActionSheet)
        
        bleCentralManager.startScan()
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .Default) { action -> Void in
            self.useCamera()            //Code for launching the camera goes here
        }
        
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Photo Library", style: .Default) { action -> Void in
            self.usePhotoLibrary()  //Code for picking from camera roll goes here
        }
        
        pickImageSheet.addAction(cancelAction)
        //Create and add first option action
        
        pickImageSheet.addAction(takePictureAction)
        //Create and add a second option action
        
        pickImageSheet.addAction(choosePictureAction)
        
        //We need to provide a popover sourceView when using it on iPad
        pickImageSheet.popoverPresentationController?.sourceView = sender as? UIView;
        
        //Present the AlertController
        self.presentViewController(pickImageSheet, animated: true, completion: nil)
    }
    
}

//MARK: UIImage Extensions for Processing

extension UIImage {
    
    func determineComponents(pos: CGPoint) -> (r: Double, g: Double, b: Double, a: Double) {
        
        let pixelData = CGDataProviderCopyData(CGImageGetDataProvider(self.CGImage))
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        
        let pixelInfo: Int = ((Int(self.size.width) * Int(pos.y)) + Int(pos.x)) * 4
        
        let r = Double(CGFloat(data[pixelInfo]) / CGFloat(255.0))
        let g = Double(CGFloat(data[pixelInfo+1]) / CGFloat(255.0))
        let b = Double(CGFloat(data[pixelInfo+2]) / CGFloat(255.0))
        let a = Double(CGFloat(data[pixelInfo+3]) / CGFloat(255.0))
        
        return (r, g, b, a)
        
    }

    func resizedImageWithBounds(bounds: CGSize) -> UIImage {
        let horizontalRatio = bounds.width / size.width
        let verticalRatio = bounds.height / size.height
        let ratio = min(horizontalRatio, verticalRatio)
        let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
        drawInRect(CGRect(origin: CGPoint.zero, size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
}