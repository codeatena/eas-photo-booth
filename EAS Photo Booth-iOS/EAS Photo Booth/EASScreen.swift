//
//  EASScreen.swift
//  EAS Photo Booth
//
//  Created by Jim Webb on 7/31/15.
//  Copyright (c) 2015 Jim Webb All rights reserved.
//

import Foundation

class EASScreen {
    let maxWidth = 240
    let maxHeight = 160
    let minWidth = 0
    let minHeight = 0
}