//
//  BLEPeripheral.swift
//  BLE
//
//  Created by 丁松 on 15/2/16.
//  Copyright (c) 2015年 丁松. All rights reserved.
//


import Foundation
import CoreBluetooth

protocol BLECentralDelegate {
    func didDiscoverConnection(connection: BLEConnection)
    func didConnectConnection(connection: BLEConnection)
}

//Init Centermanager -> scan peripheral -> connact peripher get connection
class BLECentralManager: NSObject, CBCentralManagerDelegate {
    var manager: CBCentralManager!
    var serviceUUIDs = [CBUUID]()
    
    var discoveredPeripheral: CBPeripheral!
    var connections = [BLEConnection]()
    
    var data: NSMutableData!
    var delegate: BLECentralDelegate!
    
    init(delegate: BLECentralDelegate) {
        super.init()
        
        manager = CBCentralManager(delegate: self, queue: nil)
        print("CentralManager is initialized")
    }
    
    func startScan() {
        if manager.state == CBCentralManagerState.PoweredOn {
            manager.scanForPeripheralsWithServices(serviceUUIDs, options: nil)
            print("start scan")
        }
    }
    func stopScan() {
        manager.stopScan()
        print("Stop scan")
    }
    func connect() {
        print("Connecting...")
        for connection in connections {
            let peripheral = connection.peripheral
            manager.connectPeripheral(peripheral, options: nil)
        }
    }
    func disconnect() {
        
        print("Disconnecting...")
        for connection in connections {
            manager.cancelPeripheralConnection(connection.peripheral)
        }
    }
    
    
    
    //----------------Central Manager delegate-------------------
    func centralManagerDidUpdateState(central: CBCentralManager) {
        switch central.state{
        case CBCentralManagerState.PoweredOn:
            print("Bluetooth is currently powered on and available to use.")
        case CBCentralManagerState.PoweredOff:
            print("Bluetooth is currently powered off.")
        case CBCentralManagerState.Unauthorized:
            print("The app is not authorized to use Bluetooth low energy.")
        default:
            print("centralManagerDidUpdateState: \(central.state)")
        }
    }
    //scan pripheral
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        print("Discovered peripheral \(peripheral.name)")
        print("Rssi: \(RSSI)")
        
        if peripheral.name != nil {
            let connection = BLEConnection(peripheral: peripheral, name: peripheral.name)
            print("add peripheral: \(peripheral.name)")
            connections.append(connection)
            delegate.didDiscoverConnection(connection)
        }
    }
    //connect peripheral
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        let connection = connections.filter { $0.peripheral == peripheral }[0]
        print("connect peripheral: \(peripheral.name)")
        delegate.didConnectConnection(connection)
        connection.discoverServices()
    }
    func centralManager(central: CBCentralManager?!, didFailToConnectPeripheral peripheral: CBPeripheral!, error: NSError!) {
        print("Failed to connect to peripheral: \(peripheral), " + error.localizedDescription)
    }
    func centralManager(central: CBCentralManager!, didDisconnectPeripheral peripheral: CBPeripheral!, error: NSError!) {
        print("CenCentalManagerDelegate didDisconnectPeripheral")
        discoveredPeripheral = nil
    }
}