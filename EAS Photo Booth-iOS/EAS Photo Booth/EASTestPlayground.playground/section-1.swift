// Playground - noun: a place where people can play

import Cocoa
import Foundation

var str = "Hello, playground"


func fibonacci(var input: Int) -> Int{
    switch input {
        case 0: return 0
        case 1: return 1
        default: return (fibonacci(input-2) + fibonacci(input-1))
    }
}

fibonacci(0)

class EASScreen {
    
    let MaxWidth = 10
    let MaxHeight = 10
    let MinWidth = 0
    let MinHeight = 0

}


let newScreen = EASScreen()

class CoOrd{
    var location = (x: Int() ,y: Int())
    var painted = false
    var alreadyVisited = false
    var nearestNeighbors = [CoOrd]()
    
    func distanceToCoord (checkCoord: CoOrd) -> Double{
        let a = abs(self.location.x  - checkCoord.location.x)
        let b = abs(self.location.y  - checkCoord.location.y)
        let c = sqrt(Double(a*a + b*b))
        return c
    }
    
    init(x: Int, y: Int){
        location.x = x
        location.y = y
    }
    
}

func findNearestUnpaintedNeighbors(toThisCoord: CoOrd, withinCoordGrid: [[CoOrd]], ofScreenSize: EASScreen){
    var foundNeighbor = false
    var stepAway = 1
    var largestX = Int()
    var largestY = Int()
    var smallestX = Int()
    var smallestY = Int()
    
    do {
        smallestX = max(toThisCoord.location.x - stepAway, ofScreenSize.MinWidth)
        largestX = min(toThisCoord.location.x + stepAway, ofScreenSize.MaxWidth)
        smallestY = max(toThisCoord.location.y - stepAway, ofScreenSize.MinHeight)
        largestY = min(toThisCoord.location.y + stepAway, ofScreenSize.MaxHeight)
        
        for var i = smallestX; i <= largestX; i++ {
            if withinCoordGrid[i][largestY].painted && !withinCoordGrid[i][largestY].alreadyVisited {
                toThisCoord.nearestNeighbors.append(withinCoordGrid[i][largestY])
                foundNeighbor = true
            }
        }
        for var j = largestY; j >= smallestY; j-- {
            if withinCoordGrid[largestX][j].painted && !withinCoordGrid[largestX][j].alreadyVisited {
                toThisCoord.nearestNeighbors.append(withinCoordGrid[largestX][j])
                foundNeighbor = true
            }
        }
        for var i = largestX; i >= smallestX; i-- {
            if withinCoordGrid[i][smallestY].painted && !withinCoordGrid[i][smallestY].alreadyVisited {
                toThisCoord.nearestNeighbors.append(withinCoordGrid[i][smallestY])
                foundNeighbor = true
            }
        }
        for var j = smallestY; j <= largestY; j++ {
            if withinCoordGrid[smallestX][j].painted && !withinCoordGrid[smallestX][j].alreadyVisited {
                toThisCoord.nearestNeighbors.append(withinCoordGrid[smallestX][j])
                foundNeighbor = true
            }
        }
        
        stepAway++

    } while !foundNeighbor && (stepAway <= max(ofScreenSize.MaxHeight, ofScreenSize.MaxWidth))
}


class RankedCoOrd: CoOrd {
    var disturbance = 0
}


//MARK:  Succesful 2 Dimensional Implementation

var grid = [[CoOrd]]()

for i in 0...newScreen.MaxWidth {
    grid.append([CoOrd]())
    for j in 0...newScreen.MaxHeight {
        grid[i].append(CoOrd(x: i, y: j))
        println("Coord, \(grid[i][j].location.x), \(grid[i][j].location.y)")
    }
}

class Motor {
}

let checkDistance = grid[0][0].distanceToCoord(grid[3][4])
println("\(checkDistance)")


var checkMe = grid[2][0].distanceToCoord(grid[4][3])

var newCoords = findNearestUnpaintedNeighbors(grid[5][5], grid, newScreen)

var xVal = grid[0][0].distanceToCoord(grid[2][4])
var xPos:Int = Int(xVal)




