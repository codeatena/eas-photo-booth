//
//  RedrawImage.swift
//  EAS Photo Booth
//
//  Created by Jim Webb on 5/29/15.
//  Copyright (c) 2015 Jim Webb All rights reserved.
//

import UIKit

protocol RedrawImageDataSource: class {
    //func dataForImage(sender: RedrawImageView) -> [(x: Int, y: Int)]?
    //func dataForImage(locations: [location]?) -> [(x: Int, y: Int)]?
    func dataForImage() -> ([(x: Int, y: Int)]?)
}


@IBDesignable class RedrawImageView: UIScrollView {
    
    typealias location = (x: Int, y: Int)
    
    weak var dataSource: RedrawImageDataSource?
    
    override func drawRect(rect: CGRect) {
        
        //if let drawingPoints = dataSource?.dataForImage(self) ?? [location]() {
        if let drawingPoints = dataSource?.dataForImage() {
        
            if !drawingPoints.isEmpty {
                print("number of points in UI: \(drawingPoints.count)")
                
                let context = UIGraphicsGetCurrentContext()
        
                CGContextSetLineWidth(context, 1.0)
                CGContextSetStrokeColorWithColor(context,UIColor.darkGrayColor().CGColor)
                CGContextMoveToPoint(context, CGFloat(drawingPoints[0].x), CGFloat(drawingPoints[0].y))
        
                for i in 1..<drawingPoints.count {
                    CGContextAddLineToPoint(context, CGFloat(drawingPoints[i].x), CGFloat(drawingPoints[i].y))
                }
            
                CGContextStrokePath(context)
            }
        }
    }

}




