//
//  RedrawIamgeViewController.swift
//  EAS Photo Booth
//
//  Created by Jim Webb on 5/30/15.
//  Copyright (c) 2015 Jim Webb All rights reserved.
//

import UIKit

class RedrawImageViewController: UIViewController, UIScrollViewDelegate, RedrawImageDataSource  {
    
    typealias location = (x: Int, y: Int)
    
    var newScreen = EASScreen()
    
//    var redrawImage: RedrawImageView! {
//        didSet {
//         redrawImage.dataSource = self
//        }
//    }
    
    func setUpFrame () {
        let borderSize = CGFloat(newScreen.maxWidth) * 0.1
        let frameSize = redrawnImageEASFrame.frame
        logoImageView.frame.size.height = borderSize
        redrawnImageEASFrame.cornerRadius = borderSize
        print("border size: \(borderSize)")
        
    }
    
    
    func dataForImage() -> [location]? {
        return drawingPoints
    }
    
    @IBOutlet weak var redrawnImageEASFrame: EASFrame!
        
    @IBOutlet weak var logoImageView: UIImageView!
    
     weak var redrawImageContainer: UIScrollView! {
        didSet {
            view.addSubview(redrawImage)
        }
    }
    
//    @IBOutlet var scrollView: UIScrollView! {
//        didSet {
//            view.addSubview(redrawImage)
//        }
//    }
    
    @IBOutlet var redrawImage: RedrawImageView! {
        didSet {
            redrawImage.dataSource = self
            redrawImage.delegate = self
        }
    }
    
    
    var drawingPoints = [location]?()
    
    var incomingGrid = [[CoOrd]]()
    
    var drawingDirectionsForBlueTooth = String()
    
    var newPath = CreatePath()
    
    //var path: CreatePath
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        print ("Incoming to VC: \(incomingGrid.count)")
        if incomingGrid.count > 0 {
            newPath.withinCoordGrid = incomingGrid
            drawingPoints = newPath.startPathBuilder()
        }
        print("Coming out of path builder: \(drawingPoints?.count)")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        redrawImage.contentSize = CGSize(width: newScreen.maxWidth, height: newScreen.maxHeight)
        redrawImage.minimumZoomScale = 0.1
        redrawImage.maximumZoomScale = 10.0
    }
    
    @IBAction func sendToBluetooth(sender: AnyObject) {
        
        let xMotor = Motor(motorAxis: .x, drift: 0)
        let yMotor = Motor(motorAxis: .y, drift: 0)
        
        drawingDirectionsForBlueTooth = newPath.createTextDirections(xMotor, yMotor: yMotor)
        print(drawingDirectionsForBlueTooth)
                
        //print("Compressed:")
        //let compressedPath = newPath.createCompressedDirections()
        //print("\(compressedPath)")
        
    }


}
